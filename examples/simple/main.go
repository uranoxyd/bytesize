package main

import (
	"fmt"

	"gitlab.com/uranoxyd/bytesize"
)

func main() {
	a := bytesize.ByteSize(1024 + 512)
	fmt.Println(a)

	b := bytesize.ByteSize(1*bytesize.KiB + 512*bytesize.B)
	fmt.Println(b)

	c, _ := bytesize.Parse("1K 512")
	fmt.Println(c)
}
