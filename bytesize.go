// Copyright 2023 David Ewelt <uranoxyd@gmail.com>
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful, but
//   WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
//   General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program. If not, see <http://www.gnu.org/licenses/>.

package bytesize

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

const (
	B   ByteSize = 1
	KiB ByteSize = 1 << (10 * iota)
	MiB
	GiB
	TiB
	PiB
	EiB
)

type ByteSizeInfo struct {
	Symbol string
	Bytes  uint64
}

var byteSizeValues []ByteSizeInfo = []ByteSizeInfo{
	{"EiB", uint64(EiB)},
	{"PiB", uint64(PiB)},
	{"TiB", uint64(TiB)},
	{"GiB", uint64(GiB)},
	{"MiB", uint64(MiB)},
	{"KiB", uint64(KiB)},
}

func getValueForSuffix(suffix string) uint64 {
	for _, info := range byteSizeValues {
		if strings.EqualFold(suffix, info.Symbol) {
			return info.Bytes
		} else if strings.EqualFold(string(suffix[0]), string(info.Symbol[0])) {
			return info.Bytes
		}
	}
	return 0
}

type ByteSize uint64

func (s *ByteSize) Parse(input string) error {
	isDigit := false

	var result uint64

	var buffer []rune
	var amount float64 = 0
	inputRunes := []rune(input)

	i := 0
	l := len(inputRunes)
	for i < l {
		r := inputRunes[i]
		if amount == 0 {
			if !isDigit && unicode.IsDigit(r) {
				buffer = append(buffer, r)
				isDigit = true
			} else if isDigit {
				if unicode.IsDigit(r) {
					buffer = append(buffer, r)
				} else {
					if value, err := strconv.ParseFloat(string(buffer), 64); err == nil {
						amount = value
						buffer = make([]rune, 0)
					}
					isDigit = false
					continue
				}
			}
		} else {
			if r == ' ' || unicode.IsDigit(r) {
				if r == ' ' {
					i++
				}
				if len(buffer) > 0 {
					suffix := string(buffer)
					result += getValueForSuffix(suffix) * uint64(amount)
					buffer = make([]rune, 0)
				}
				amount = 0
				continue
			} else {
				buffer = append(buffer, r)
			}
		}

		i++
	}

	if isDigit {
		if value, err := strconv.ParseFloat(string(buffer), 64); err == nil {
			result += uint64(value)
		}
	} else if len(buffer) > 0 {
		suffix := string(buffer)
		result += getValueForSuffix(suffix) * uint64(amount)
	}

	*s = ByteSize(result)

	return nil
}

// UnmarshalText implements encoding.TextUnmarshaler interface
func (s *ByteSize) UnmarshalText(text []byte) error {
	return s.Parse(string(text))
}

func (s ByteSize) toString() string {
	for _, info := range byteSizeValues {
		if s >= ByteSize(info.Bytes) {
			return fmt.Sprintf("%.02f %s", float64(s)/float64(info.Bytes), info.Symbol)
		}
	}
	return fmt.Sprintf("%d B", s)
}
func (s ByteSize) String() string {
	return s.toString()
}

func Parse(input string) (s ByteSize, err error) {
	err = s.Parse(input)
	return
}
